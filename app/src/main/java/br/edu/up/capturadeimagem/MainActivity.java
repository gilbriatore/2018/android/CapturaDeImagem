package br.edu.up.capturadeimagem;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

  ImageView imgv;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    imgv = (ImageView) findViewById(R.id.imageView);
  }

  public void onClickCapturar(View v){

    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    startActivityForResult(intent, 0);

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (data != null) {
      Bundle bundle = data.getExtras();
      if (bundle != null) {
        Bitmap bmp = (Bitmap) data.getExtras().get("data");
        imgv.setImageBitmap(bmp);
      }
    }
  }
}
